## General Information

Spiderum is the one of the best blogging platform in Vietnam with 40.000 users, 30.000
articles and approximately 500.000 monthly unique users. We are looking for qualified
software developers to join and develop Spiderum to become the top-of-mind destination for
the top minds of Vietnam to share and exchange ideas/knowledge in a constructive manner.

## Location

* Hanoi, Vietnam

## Salary Expectation

* Negotiable during interview

## Requirements

* Strong proficiency with JavaScript (Knowledge of TypeScript is a bonus)
* Understanding the nature of asynchronous programming and its quirks and workarounds
* Good understanding of HTML5 and CSS3. Have knowledge about CSS preprocessor such as SASS is a bonus.
* Good basic knowledge of Database system (both relational database and nosql database)
* Understanding of automated testing platforms and unit tests
* Proficient understanding of code versioning tools, such as Git
* Knowledge of Node.js and Express frameworks is a bonus
* Knowledge of React.js, Angular is a bonus

### Responsibilities

Maintaining and improving Spiderum.com; Spiderum Mobile App & other products by
completing the following tasks:
* Integration of user-facing elements developed by front-end developers with server side
logic (including website and mobile app)
* Implementation of security and data protection
* Writing reusable, testable, and efficient code
* Create features and eliminate “features”

### Contact (MUST)
Send your CV & related materials to contact@spiderum.com
